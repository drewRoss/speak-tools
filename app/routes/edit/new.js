import websites from "appkit/models/websites";

var EditNewRoute = Ember.Route.extend({
    setupController: function (controller, model) {
        console.log("~~~~~~~~~~~~~ NEW SITE ~~~~~~~~~~~~~~~");
        console.log("~~~~~~~~~~~~~ Controller: " + controller + " model: " + model);
        this.controllerFor('edit.site').setProperties({isNew:false, model: {}});
    },
    renderTemplate: function () {
        console.log("~~~~~~~~~~~~~ REDIRECT TO EDIT ~~~~~~~~~~~~~~~");
        //this.render('new');
        //this.render('edit');
        //this.render('edit.new');
        //this.render('site');
        this.render('edit.site'); //(works via edit back to manage)
    }
});

export default EditNewRoute;
