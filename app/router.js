var Router = Ember.Router.extend(); // ensure we don't share routes between all Router instances

Router.map(function() {
    this.route('component-test');
    this.route('helper-test');
    this.route("manage", { path: "/manage" });
    this.resource("edit", function () {
      this.route('site', { path: "/:site_id" });
      this.route('new');
    });
});

export default Router;
