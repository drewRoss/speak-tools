var allServices = [

    {routeName:'manage',    routeLabel:'Website Manager'},
    {routeName:'',          routeLabel:'Place Manager'},
    {routeName:'',          routeLabel:'Agency Manager'},
    {routeName:'',          routeLabel:'Resource Manager'},
    {routeName:'',          routeLabel:'Registered Agent Manager'},
    {routeName:'',          routeLabel:'Product Manager'},
    {routeName:'',          routeLabel:'Document Manager'}

];

export default allServices;