
DATE | 3/8/2014

================== APP

Login / Logout

  Admin Section

      Website Manager
        Contact Info
        Billing Info
        Products
   
      Place Manager
        Add State
   
      Agency Manager
      Entity Manager
        add entity type
   
      Resource Manager
        Add Resource
        Edit Resource
        List Resources
   
      Registered Agent Manager
        Add Agent
          Assign Resources
        Edit Agent
        List Agents
   
      Product Manager
        Add Product
        Edit Product
        List Products
   
      Document Manager
        Client Documents
          Category Manager
          View uploaded from local office
   
    Client Section
      Resources
      Documents
      Reminders
      Add Services
      Account Details

      Local Office
        Upload Document

================== NODE|BOWER SETUP

(new machine)
sudo npm install -g grunt-cli
sudo npm install -g bower
sudo npm install -g less
sudo npm install --save-dev grunt-contrib-less

(generators)
sudo npm install -g loom-generators-ember-appkit --save

(deploy tools)
sudo npm install -g grunt-usemin
sudo npm install -g grunt-rev

================== EAK

aross$ (clone appkit)
  cd ~/dev, git clone https://github.com/stefanpenner/ember-app-kit.git {project-name}, cd ~/dev/{project-name}

aross$ (remove git, create own)
  rm -rf .git, git init, git remote add origin https://{username}@bitbucket.org/{username}}/{projectname}.git

aross$ (install npm)
  npm install

aross$ (start server)
  grunt server, chrome -> http://localhost:8000/

================== OSX

sudo apachectl start (apache)
ln -s /path to sublime/ sudo !! (symlink from /usr/local/bin)
ember inspector - command+option+j

================== BITBUCKET

BitBUCKET
  https://arllcagent@bitbucket.org/

Bitbucket path convention:
  https://arllcagent@bitbucket.org/arllcagent/corptools/admin.git
  https://arllcagent@bitbucket.org/corptools/corptools-backend.git
  https://arllcagent@bitbucket.org/corptools/corptools-frontend.git

  (setup)
  git remote add origin https://arllcagent@bitbucket.org/arllcagent/corptools.git
  git add .
  git status
  git commit -m "initial commit"
  git push origin master

  (commit)
  git add . || --all (to delete)
  git commit -m "readme update"
  git push origin master

  (checkout)
  git pull origin master (checkout)

  git remote add origin 

  (checkin)
  cd /local-repo
  git remote add origin https://arllcagent@bitbucket.org/arllcagent/appkit.git
  git push -u origin --all # pushes up the repo and its refs for the first time
  git push -u origin --tags # pushes up any tags


================== LINKS

installs:
  http://nodejs.org/download/

reading:
  http://bower.io/
  http://emberjs.com/guides/
  http://getbootstrap.com/css/
  http://stackoverflow.com/questions/15624593/inserting-a-model-property-into-an-img-element-url-in-ember
  http://stackoverflow.com/questions/20684879/ember-and-external-js-scripts
  http://stackoverflow.com/questions/15017405/using-jquery-in-ember-app
  http://embersherpa.com/articles/crud-example-app-without-ember-data/
  http://q42.nl/blog/post/35203391115/debug-sass-and-less-in-webkit-inspector-and-save-css-cha
  https://www.openshift.com/blogs/day-1-bower-manage-your-client-side-dependencies
  http://www.abeautifulsite.net/blog/2013/08/whipping-file-inputs-into-shape-with-bootstrap-3/
  http://labs.abeautifulsite.net/demos/bootstrap-file-inputs/
  http://stackoverflow.com/questions/15094667/compile-less-files-with-grunt-contrib-less-wont-work
  http://ericnishio.com/blog/compile-less-files-with-grunt
  http://discuss.emberjs.com/t/best-practice-for-using-bound-ember-templates-along-with-bootstrap-components/3801
  http://readwrite.com/2013/09/30/understanding-github-a-journey-for-beginners-part-1
  http://matthewlehner.net/ember-js-routing-and-views/
  http://brew.sh/
  http://iamstef.net/ember-app-kit/

================== GITS

https://github.com/emberjs/website/trunk/source/guides/
https://github.com/cavneb/loom-generators-ember-appkit
https://github.com/gruntjs/grunt-contrib-less
https://github.com/stefanpenner/ember-app-kit
https://github.com/stefanpenner/ember-app-kit/blob/master/app/index.html#L24

================== DEPLOY

grunt build:dist
grunt build:server

(push to bb)

git add . 
git commit -m "mesesage"
git push origin master

chrome -> http://corporatetools.com/

================== NOTES

for eak, .less files live adjacent to /styles/.css, (grunt-contrib-less auto reloads)
sudo bower install bootstrap --save


