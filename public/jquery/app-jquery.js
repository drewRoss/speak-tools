// Display Selected File Name on Custom File Upload Fields
$(document).on('change', '.btn-file :file', function() {
    var input = $(this),
        numFiles = input.get(0).files ? input.get(0).files.length : 1,
        label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
    input.trigger('fileselect', [numFiles, label]);
    // typically on fileselect (see bewlo)
    var select_spot = $(this).parent().siblings(".btn-file-selected");
    var origin_text = select_spot.text();
    if(label!==""){
        select_spot.text(label);
    }
});